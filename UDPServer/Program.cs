﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Sockets;
using System.Net;
using System.Security.AccessControl;
using System.Threading;
using System.Threading.Tasks;

namespace UDPServer
{
    //ENUMy z kodami operacji i odpowiedzi
    enum OperationType { GetSession = 1, Invite = 2, InviteResponse = 3, Message = 4, Disconnect = 5, Ack = 7 };
    enum SessionEnum { Request = 0, GotSession = 1, Offer = 2 };
    enum InviteEnum { Send = 0, Sent = 1, Error = 2 };
    enum InviteResponseEnum { Accept = 0, Deny = 1, Accepted = 2, Denied = 3 };
    enum MessageEnum { Send = 0, Sent = 1, Error = 2 };
    enum DisconnectEnum { Request = 0, Success = 1, Disconnected = 2 };

    //klasa przechowująca informacje o połączeniach z klientami
    public class Guests
    {
        public Guests(IPEndPoint firstGuest, IPEndPoint secondGuest)
        {
            this.FirstGuest = firstGuest;
            this.SecondGuest = secondGuest;
            Connected = false;
        }
        public IPEndPoint FirstGuest { get; set; }
        public IPEndPoint SecondGuest { get; set; }
        public bool Connected;

        public void SetConnected(bool val)
        {
            Connected = val;
        }
        public bool IsBothSet()
        {
            if (FirstGuest != null && SecondGuest != null) return true;
            else return false;
        }
    }

    //główna klasa
    internal class Program
    {
        protected int ListeningPort;
        public Dictionary<int, Guests> SessionsList;
        public int SSIDHolder;
        private List<IPAddress> acqChecker;
        private List<Task> tasks;
        private List<Task> tasks2;
        private UdpClient udpServer;
        private IPEndPoint receiverEndpoint;
        public Program()
        {
            SessionsList = new Dictionary<int, Guests>();
            SSIDHolder = 0;
            acqChecker = new List<IPAddress>();
            tasks = new List<Task>();
            tasks2 = new List<Task>();
            Console.Write("Podaj port nasłuchiwania: ");
            ListeningPort = Convert.ToInt32(Console.ReadLine()?.Trim());
            //udpServer = new UdpClient(ListeningPort, AddressFamily.InterNetwork);
            receiverEndpoint = new IPEndPoint(IPAddress.Any, ListeningPort);
        }


        private static void Main()
        {
            Program p = new Program();
            //Task runner = p.Run();
            //runner.Wait();
            p.Run();
        }



        protected void Run()
        {

            //var data = udpServer.Receive(ref receiverEndpoint);
            //DataProcessing(data, receiverEndpoint);
            using (udpServer = new UdpClient(ListeningPort))
            {
                //odbieranie pakietów i wywołanie funkcji przetwarzającej dane
                while (true)
                {
                    var result = udpServer.Receive(ref receiverEndpoint);
                    DataProcessing(result, receiverEndpoint);

                }
            }



        }

        //wykonywanie odpowiednich operacji na podstawie otrzzymanego pakietu
         void DataProcessing(byte[] data, IPEndPoint receiverEndPoint)
        {
            //odczytywanie pól operacji i odpowiedzi z otrzymanego pakietu
            string operation = Convert.ToString(data[0], 2).PadLeft(8, '0').Substring(0, 3);
            string response = Convert.ToString(data[0], 2).PadLeft(8, '0').Substring(3, 3);
#if DEBUG
            Console.Write("received: " + Convert.ToString(data[0], 2).PadLeft(8,'0'));
            foreach (var acq in acqChecker)
            {
                Console.Write(" " + acq);
            }
            Console.Write("\n");
#endif
            //otrzymanie komunikatu ACK, 111 000
            if (Convert.ToString((int)OperationType.Ack, 2).PadLeft(3, '0') == operation)
            {
                //if (acqChecker.Count > 0) acqChecker.Remove(acqChecker.Last());
                if (acqChecker.Contains(receiverEndPoint.Address))
                {
                    //usuwanie klienta z listy oczekiwania na ACK
                    acqChecker.Remove(receiverEndPoint.Address);
                }
                else
                {
                    Console.WriteLine("Nieoczekiwany błąd. Otrzymano potwierdzenie otrzymania niewysłanego pakietu!");
                    var ssid = GetSessionIdFromData(data);
                    if (receiverEndPoint == SessionsList[ssid].SecondGuest) SessionsList[ssid].SecondGuest = null;
                    else if (receiverEndPoint == SessionsList[ssid].FirstGuest) SessionsList[ssid].FirstGuest = null;
                    else Console.WriteLine("Błąd krytyczny");
                }
            }
            else
            {
                //wysłanie ACK gdy otrzymany komunikat nie jest ACK
                var ssid = GetSessionIdFromData(data);
                Send(OperationType.Ack, 0, receiverEndPoint, ssid, 1200);
            }

            //odczytywanie danych jeśli nie oczekujemy na ACK od danego klienta
            if (!acqChecker.Contains(receiverEndPoint.Address))
            {
                #region odbiór danych

                //operacja 001, uzyskanie sesji
                if (Convert.ToString((int)OperationType.GetSession, 2).PadLeft(3, '0') == operation)
                {
                    //operacja 001 odpowiedź 001, klient dostał id sesji
                    //dodawanie klienta do istniejącej sesji lub tworzenie nowej
                    if (Convert.ToString((int)SessionEnum.GotSession, 2).PadLeft(3, '0') == response)
                    {
                        if (GetSessionIdFromData(data) == SSIDHolder)
                        {
                            if (SessionsList.Where(p => p.Key == SSIDHolder).Any())
                            {
                                if (SessionsList[SSIDHolder].FirstGuest == null)
                                {
                                    //SessionsList[SSIDHolder].SetFirst(ReceiverEndPoint);
                                    SessionsList[SSIDHolder] = new Guests(receiverEndPoint,
                                        SessionsList[SSIDHolder].SecondGuest);
                                }
                                else
                                {
                                    //SessionsList[SSIDHolder].SetSecond(ReceiverEndPoint);
                                    SessionsList[SSIDHolder] = new Guests(SessionsList[SSIDHolder].FirstGuest,
                                        receiverEndPoint);
#if DEBUG
                                    Console.WriteLine(SSIDHolder.ToString());
#endif
                                }
                            }
                            else
                            {
                                SessionsList.Add(SSIDHolder, new Guests(receiverEndPoint, null));
#if DEBUG
                                Console.WriteLine(SSIDHolder.ToString());
#endif
                            }

                            SSIDHolder = 0;
                        }
                        else
                        {
                            Console.WriteLine("Otrzymano inny numer sesji");
                        }


                    }
                    //operacja 001 odpowiedź 000, klient prosi o id sesj 
                    else if (Convert.ToString((int)SessionEnum.Request, 2).PadLeft(3, '0') == response)
                    {
                        //wysyłanie id istniejącej sesji klientowi lub generowanie nowego id
                        if (SSIDHolder == 0)
                        {
                        if (SessionsList.Any(p => p.Value.FirstGuest == null || p.Value.SecondGuest == null))
                        {
                            var ssidNumerable = SessionsList
                                .Where(p => p.Value.FirstGuest == null || p.Value.SecondGuest == null)
                                .Select(p => p.Key);
                            if (ssidNumerable.Count() > 0)
                            {
                                SSIDHolder = ssidNumerable.First();
                            }
                            else
                            {
                                SSIDHolder = new Random().Next(1, 255);
                            }
                        }
                        else SSIDHolder = new Random().Next(1, 255);

                        //operacja 001 odpowiedź 010, wysłanie id sesji
                        Send(OperationType.GetSession, (int)SessionEnum.Offer, receiverEndPoint, SSIDHolder, 1200);

                        }
                        else Console.WriteLine("Obecnie serwer jest zajęty przypisywaniem innej sesji");

                    }
                }

                //operacja 010, klient zaprasza
                if (Convert.ToString((int)OperationType.Invite, 2).PadLeft(3, '0') == operation)
                {
                    var ssid = GetSessionIdFromData(data);
                    if (SessionsList.Any(p => p.Key == GetSessionIdFromData(data) && p.Value.IsBothSet() == true))
                    {

                        if (SessionsList[ssid].FirstGuest.Equals(receiverEndPoint))
                        {
                            //operacja 010 odpowiedź 000, przekaż zaproszenie
                            Send(OperationType.Invite, (int)InviteEnum.Send, SessionsList[ssid].SecondGuest, ssid,
                                1200);
                            //operacja 010 odpowiedź 010, potwierdzenie wysłania zaproszenia
                            Send(OperationType.Invite, (int)InviteEnum.Sent, receiverEndPoint, ssid, 1200);
                        }
                        else
                        {
                            //operacja 010 odpowiedź 000, przekaż zaproszenie
                            Send(OperationType.Invite, (int)InviteEnum.Send, SessionsList[ssid].FirstGuest, ssid,
                                1200);
                            //operacja 010 odpowiedź 001, potwierdzenie wysłania zaproszenia
                            Send(OperationType.Invite, (int)InviteEnum.Sent, receiverEndPoint, ssid, 1200);
                        }
                    }
                    else
                    {
                        //sendError("Nie znaleziono drugiego użytkownika w sesji", ReceiverEndPoint, 1200);
                        Send(OperationType.Invite, (int)InviteEnum.Error, receiverEndPoint, ssid, 1200);
                    }
                }

                //operacja 011, odpowiedź na zaproszenie
                if (Convert.ToString((int)OperationType.InviteResponse, 2).PadLeft(3, '0') == operation)
                {
                    var ssid = GetSessionIdFromData(data);
                    if (SessionsList.ContainsKey(ssid))
                    {
                        if (SessionsList[ssid].IsBothSet())
                        {
                            //operacja 011 odpowiedź 000, akceptacja
                            if (Convert.ToString((int)InviteResponseEnum.Accept, 2).PadLeft(3, '0') == response)
                            {
                                SessionsList[ssid].SetConnected(true);
                                //operacja 011 odpowiedź 010, potwierdzenie akceptacji
                                Send(OperationType.InviteResponse, (int)InviteResponseEnum.Accepted,
                                    SessionsList[ssid].FirstGuest, ssid, 1200);
                                Send(OperationType.InviteResponse, (int)InviteResponseEnum.Accepted,
                                    SessionsList[ssid].SecondGuest, ssid, 1200);
                            }
                            //operacja 011 odpowiedź 001, odrzucenie 
                            else if (Convert.ToString((int)InviteResponseEnum.Deny, 2).PadLeft(3, '0') == response)
                            {
                                //operacja 011, odpowiedź 011, potwierdzenie odrzucenia
                                Send(OperationType.InviteResponse, (int)InviteResponseEnum.Denied,
                                    SessionsList[ssid].FirstGuest, ssid, 1200);
                                Send(OperationType.InviteResponse, (int)InviteResponseEnum.Denied,
                                    SessionsList[ssid].SecondGuest, ssid, 1200);
                            }
                        }
                        else
                        {
                            //sendError("Nie znaleziono drugiego użytkownika w sesji", ReceiverEndPoint, 1200);
                            Send(OperationType.Invite, (int)InviteEnum.Error, receiverEndPoint, ssid, 1200);
                        }
                    }
                    else
                    {
                        Console.Write("Otrzymano inny ID sesji");
                    }
                }



                //operacja 100, wysłanie wiadomości
                if (Convert.ToString((int)OperationType.Message, 2).PadLeft(3, '0') == operation)
                {

                    var ssid = GetSessionIdFromData(data);
                    //przekazywanie wiadomości i wysyłanie potwierdzenia do odpowiedniego klienta
                    if (SessionsList[ssid].FirstGuest.Equals(receiverEndPoint))
                    {
                        //100 000
                        SendMessage((int)MessageEnum.Send, data, SessionsList[ssid].SecondGuest, ssid);
                        //100 001
                        SendMessage((int)MessageEnum.Sent, data, SessionsList[ssid].FirstGuest, ssid);
                    }
                    else if (SessionsList[ssid].SecondGuest.Equals(receiverEndPoint))
                    {
                        //100 000
                        SendMessage((int)MessageEnum.Send, data, SessionsList[ssid].FirstGuest, ssid);
                        //100 001
                        SendMessage((int)MessageEnum.Sent, data, SessionsList[ssid].SecondGuest, ssid);
                    }
                    else
                    {
                        Console.WriteLine("Brak tego hosta w sesji lub błąd w sesji");
                        //100 010
                        Send(OperationType.Message, (int)MessageEnum.Error, receiverEndPoint, ssid, 1200);
                    }
                }

                //operacja 101 odpowiedź 000, rozłączanie
                if (Convert.ToString((int)OperationType.Disconnect, 2).PadLeft(3, '0') == operation)
                {
                    var ssid = GetSessionIdFromData(data);
                    if (SessionsList.ContainsKey(ssid))
                    {
                        //wysyłanie potwierdzenia rozłączenia do klienta i rozłączanie drugiego
                        if (SessionsList[ssid].FirstGuest.Equals(receiverEndPoint))
                        {
                            Send(OperationType.Disconnect, (int)DisconnectEnum.Success, SessionsList[ssid].FirstGuest, ssid, 1200);
                            if(SessionsList[ssid].SecondGuest != null) Send(OperationType.Disconnect, (int)DisconnectEnum.Disconnected, SessionsList[ssid].SecondGuest, ssid, 1200);
                        }
                        else
                        {
                            Send(OperationType.Disconnect, (int)DisconnectEnum.Success, SessionsList[ssid].SecondGuest, ssid, 1200);
                            if (SessionsList[ssid].FirstGuest != null) Send(OperationType.Disconnect, (int)DisconnectEnum.Disconnected, SessionsList[ssid].FirstGuest, ssid, 1200);
                        }
                        //usuwanie sesji z listy
                        SessionsList.Remove(ssid);
                    }
                    else
                    {
                        Console.WriteLine("Błędny indentyfikator sesji");
                    }
                }

                #endregion
            }
            else
            {
                var ssid = GetSessionIdFromData(data);
                Console.WriteLine("Otrzymano pakiet bez potwierdzenia. Rozłącznie klienta.");
                if (receiverEndPoint == SessionsList[ssid].SecondGuest) SessionsList[ssid].SecondGuest = null;
                else if (receiverEndPoint == SessionsList[ssid].FirstGuest) SessionsList[ssid].FirstGuest = null;
                else Console.WriteLine("Błąd krytyczny");
            }

        }

        //odczytywanie pola id sesji
        public int GetSessionIdFromData(byte[] data)
        {
            StringBuilder sb = new StringBuilder();
            int messageLength = GetMessageLengthFromData(data);
            sb.Append(Convert.ToString(data[4 + messageLength], 2).PadLeft(8, '0').Substring(6, 2));
            sb.Append(Convert.ToString(data[5 + messageLength], 2).PadLeft(8, '0').Substring(0, 6));
            return Convert.ToInt32(sb.ToString(), 2);
        }

        #region metody wysyłające
        private void Send(OperationType operation, int response, IPEndPoint receiverEndPoint, int sessionId, int port)
        {
            //budowanie komunikatu
            StringBuilder sb = new StringBuilder();
            //pole operacji
            sb.Append(Convert.ToString((int)operation, 2).PadLeft(3, '0')); 
            //pole odpowiedzi
            sb.Append(Convert.ToString(response, 2).PadLeft(3, '0'));
            //pole długości danych (0 - ponieważ nie jest to wiadomość)
            for (int i = 0; i < 32; i++)
            {
                sb.Append("0");
            }
            //id sesni
            var sessionIdArr = Convert.ToByte(sessionId);
            sb.Append(Convert.ToString(sessionIdArr, 2).PadLeft(8, '0'));
            //dopełnienie do pełnego bajtu
            sb.Append("00");
            string binaryString = sb.ToString();
            int bytesNumber = sb.Length / 8;
            byte[] dg = new byte[bytesNumber];
            for (int i = 0; i < bytesNumber; i++)
            {
                dg[i] = Convert.ToByte(binaryString.Substring(i * 8, 8), 2);
            }
            using (UdpClient sender = new UdpClient(1400))
            {
                //wysyłanie pakietu
                sender.Send(dg, dg.Length, new IPEndPoint(receiverEndPoint.Address, port));
                //jeśli nie jest to ACK - dodawanie klienta do listy oczekiwania na ACK
                if (operation != OperationType.Ack) acqChecker.Add(receiverEndPoint.Address);
            }

#if DEBUG
            Console.WriteLine("sent: " + Convert.ToString(dg[0],2).PadLeft(8,'0') + " " + acqChecker.Count);
#endif
        }

        private void SendMessage(int response, byte[] data, IPEndPoint receiverEndPoint, int ssid)
        {
            //budowanie komunikatu
            string message = GetMessageFromData(data);
            StringBuilder sb = new StringBuilder();
            //pola operacji i odpowiedzi
            sb.Append(Convert.ToString((int)OperationType.Message, 2).PadLeft(3, '0')); 
            sb.Append(Convert.ToString(response, 2).PadLeft(3, '0')); 
            //długość wiadomości
            sb.Append(Convert.ToString(message.Length, 2).PadLeft(32, '0'));
            //wiadomość
            byte[] messageBytes = Encoding.ASCII.GetBytes(message);
            foreach (byte b in messageBytes)
            {
                sb.Append(Convert.ToString(b, 2).PadLeft(8, '0'));
            }
            //id sesji
            var sessionIdArr = Convert.ToByte(ssid);
            sb.Append(Convert.ToString(sessionIdArr, 2).PadLeft(8, '0'));
            //dopełnienie do pełnego bajtu
            sb.Append("00");
            string binaryString = sb.ToString();
            int bytesNumber = sb.Length / 8;
            byte[] dg = new byte[bytesNumber];
            for (int i = 0; i < bytesNumber; i++)
            {
                dg[i] = Convert.ToByte(binaryString.Substring(i * 8, 8), 2);
            }
            using (UdpClient sender = new UdpClient(1400))
            {
                //wysłanie wiadomości
                sender.Send(dg, dg.Length, new IPEndPoint(receiverEndPoint.Address, 1200));
                //dodanie klienta do listy oczekiwania na ACK
                acqChecker.Add(receiverEndPoint.Address);
            }
        }


        #endregion



        //odczytywanie pola długości danych
        private int GetMessageLengthFromData(byte[] data)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(Convert.ToString(data[0], 2).PadLeft(8, '0').Substring(6, 2));
            for (int i = 1; i < 4; i++) sb.Append(Convert.ToString(data[i], 2).PadLeft(8, '0'));
            sb.Append(Convert.ToString(data[4], 2).PadLeft(8, '0').Substring(0, 6));
            return Convert.ToInt32(sb.ToString(), 2);
        }

        //odczytywanie wiadomości
        private string GetMessageFromData(byte[] data)
        {
            StringBuilder sb = new StringBuilder();
            int bytesToRead = GetMessageLengthFromData(data);
            for (int i = 4; i < 4 + bytesToRead; i++)
            {
                sb.Append(Convert.ToString(data[i], 2).PadLeft(8, '0').Substring(6, 2));
                sb.Append(Convert.ToString(data[i + 1], 2).PadLeft(8, '0').Substring(0, 6));
            }
            string messageBits = sb.ToString();
            List<byte> bytesList = new List<byte>();
            for (int i = 0; i < bytesToRead; i++)
            {
                bytesList.Add(Convert.ToByte(messageBits.Substring(8 * i, 8), 2));
            }
            string message = Encoding.ASCII.GetString(bytesList.ToArray());
#if DEBUG
            Console.WriteLine(message);
#endif
            return message;

        }

    }
}
